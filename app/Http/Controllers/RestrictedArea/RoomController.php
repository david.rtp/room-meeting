<?php

namespace App\Http\Controllers\RestrictedArea;

use App\Http\Requests\RestrictedArea\RoomRequest;
use App\Http\Requests\RestrictedArea\RoomUpdateRequest;
use App\Repositories\RoomRepository;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoomController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new RoomRepository();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setMenu(MENU_ROOM);
        $rooms = $this->repository->getRoomsPaginate();
        return view('restricted-area.room.index', compact('rooms'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setMenu(MENU_ROOM);
        return view('restricted-area.room.create');
    }

    /**
     * @param RoomRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoomRequest $request)
    {
        $this->repository->storeRoom($request);
        return redirect()->route('restricted-area.room.index')->with(['message' => 'Sala criada com sucesso.']);
    }

    /**
     * @param $roomId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($roomId)
    {
        $this->setMenu(MENU_ROOM);
        $room = $this->repository->getRoomById($roomId);
        if(!$room){
            throw new ModelNotFoundException('Sala não encontrada');
        }
        return view('restricted-area.room.show', compact('room'));
    }

    /**
     * @param $roomId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($roomId)
    {
        $this->setMenu(MENU_ROOM);
        $room = $this->repository->getRoomById($roomId);
        if(!$room){
            throw new ModelNotFoundException('Sala não encontrada');
        }
        return view('restricted-area.room.edit', compact('room'));
    }

    /**
     * @param RoomUpdateRequest $request
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoomUpdateRequest $request, $roomId)
    {
        $this->repository->updateRoom($roomId, $request);
        return redirect()->route('restricted-area.room.edit', $roomId)->with(['message' => 'Sala atualizada com sucesso.']);
    }

    /**
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($roomId)
    {
        $this->repository->deleteRoom($roomId);
        return redirect()->route('restricted-area.room.index')->with(['message' => 'Sala excluída com sucesso.']);
    }
}

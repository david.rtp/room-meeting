<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array $sectionsName
     */
    protected function setSection($sectionsName = array()){
        foreach ($sectionsName as $sectionName => $value){
            session()->forget($sectionName);
            session()->put($sectionName, $value);
        }
    }

    /**
     * @param null $menuSection
     */
    protected function setMenu($menuSection = null){
        $sectionName = [SECTION_MENU => $menuSection];
        $this->setSection($sectionName);
    }
}

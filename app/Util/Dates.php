<?php
/**
 * Created by PhpStorm.
 * User: David Pereira
 * Date: 07/03/2017
 * Time: 17:44
 */

namespace App\Util;

use Carbon\Carbon;

trait Dates
{
    /**
     * @param $dateIni
     * @param $dateFim
     * @return int
     */
    public static function diffDays($dateIni, $dateFim){
        $dateIni = Carbon::parse($dateIni);
        $dateFim = Carbon::parse($dateFim);
        return $dateFim->diffInDays($dateIni); // retornando a diferença de dias entre as datas
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @return int
     */
    public function diffInHours($dateStart, $dateEnd){
        $dateStart = Carbon::parse($dateStart);
        $dateEnd = Carbon::parse($dateEnd);
        return $dateStart->diffInHours($dateEnd);
    }

    /**
     * @param $daysAdd
     * @param null $dateStart
     * @return string
     */
    public function addDays($daysAdd, $dateStart = null){
        $dateStart = is_null($dateStart) ? Carbon::now() : $dateStart;
        $date = Carbon::parse($dateStart)->addDay($daysAdd);
        return $date->toDateString();
    }

    /**
     * @param $daysAdd
     * @param null $dateStart
     * @return string
     */
    public function subDays($daysSub, $dateStart = null){
        $dateStart = is_null($dateStart) ? Carbon::now() : $dateStart;
        $date = Carbon::parse($dateStart)->subDay($daysSub);
        return $date->toDateString();
    }

    /**
     * @param $dateStart
     * @param $weekAdd
     * @return string
     */
    public function addWeeks($dateStart, $weekAdd, $time = false){
        $date = Carbon::parse($dateStart)->addWeeks($weekAdd);
        return !$time ? $date->toDateString(): $date->toDateTimeString();
    }

    /**
     * @param $dateStart
     * @param $weekSub
     * @return string
     */
    public function subWeeks($dateStart, $weekSub){
        $date = Carbon::parse($dateStart)->subWeeks($weekSub);
        return $date->toDateString();
    }

    /**
     * @param $dateStart
     * @param $monthAdd
     * @return string
     */
    public function addMonths($dateStart, $monthAdd){
        $date = Carbon::parse($dateStart)->addMonths($monthAdd);
        return $date->toDateString();
    }

    /**
     * @param $dateStart
     * @param $monthSub
     * @return string
     */
    public function subMonths($dateStart, $monthSub){
        $date = Carbon::parse($dateStart)->subMonths($monthSub);
        return $date->toDateString();
    }

    /**
     * @param null $date
     * @return int
     */
    public function getWeek($date = null){
        $date = is_null($date) ? Carbon::now() : $date;
        $date = Carbon::parse($date);
        return $date->dayOfWeek + 1; // Sunday = 0
    }

    /**
     * @param boolean $hour
     * @param boolean $onlyHours
     * @return string
     */
    public static function now($hour = false, $onlyHours = false){
        if($hour){
            return $onlyHours ? Carbon::now()->toTimeString() : Carbon::now()->toDateTimeString();
        }
        return Carbon::now()->toDateString();
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public function parseDate($date, $format = 'Y-m-d H:i:s'){
        return Carbon::parse($date)->format($format);
    }

    /**
     * @param $date
     * @return bool
     */
    public static function isPast($date){
        $date = Carbon::parse($date);
        return $date->isPast();
    }

    /**
     * @param $date
     * @return bool
     */
    public static function noPast($date){
        $date = Carbon::parse($date);
        return ($date->isToday() || $date->isFuture()) ? true : false;
    }

    /**
     * @param $date
     * @return bool
     */
    public function isToday($date){
        $date = Carbon::parse($date);
        return $date->isToday();
    }

    /**
     * @param null $date
     * @return string
     */
    public static function firstDayOfMonth($date = null){
        $date = is_null($date) ? Carbon::now() : $date;
        return Carbon::parse($date)->startOfMonth()->toDateString();
    }

    /**
     * @param null $date
     * @return string
     */
    public static function lastDayOfMonth($date = null){
        $date = is_null($date) ? Carbon::now() : $date;
        return Carbon::parse($date)->lastOfMonth()->toDateString();
    }

    /**
     * @return int
     */
    public static function currentYear(){
        return Carbon::now()->year;
    }

    /**
     * @param null $date
     * @return int
     */
    public static function currentMonth($date = null){
        if(is_null($date)){
            return Carbon::now()->month;
        }
        return Carbon::parse($date)->month;
    }

    /**
     * @param null $year
     * @param null $month
     * @param null $day
     * @return string
     */
    public static function createFromDate($year = null, $month = null, $day = null){
        return Carbon::createFromDate($year, $month, $day)->toDateString();
    }

    /**
     * @param $date
     * @param string $fromFormat
     * @param string $format
     * @return string
     */
    public static function convertDate($date, $fromFormat = 'd/m/Y', $format = 'Y-m-d'){
        return Carbon::createFromFormat($fromFormat, $date)->format($format);
    }

    /**
     * @param $date
     * @param int $minutes
     * @param boolean $time
     * @return string
     */
    public function addMinutes($date, $minutes = 0, $time = false){
        $dt = Carbon::parse($date);
        return $time ? $dt->addMinutes($minutes)->toTimeString() : $dt->addMinutes($minutes)->toDateTimeString();
    }


    /**
     * @param $time
     * @return int
     */
    public static function convertHourToMinutes($time){
        $hourParse = Carbon::parse($time);
        $hours = $hourParse->hour;
        $minutes = $hourParse->minute;
        $totalMinutes = $minutes + ($hours * 60);
        return $totalMinutes;
    }

    /**
     * @param $minutes
     * @param string $format
     * @return string
     */
    public function convertMinutesToHours($minutes, $format = 'H:i:s'){
        $hourInit = Carbon::parse('00:00:00')->addMinutes($minutes);
        return $hourInit->format($format);
    }

    /**
     * @param null $date
     * @param string $fromFormat
     * @return int
     */
    public static function getWeekDay($date = null, $fromFormat = 'd/m/Y')
    {
        $date = is_null($date) ? Carbon::now() : $date;
        if(!($date instanceof Carbon)){
            $date = Carbon::createFromFormat($fromFormat, $date);
        }
        return $date->dayOfWeek + 1; // Sunday is 0
    }

    /**
     * @description $date format Y-m-d HH:mm:ss
     * @param string $date
     * @param int $hours
     * @param boolean $onlyHours
     * @return string
     */
    public function addHours($date, $hours, $onlyHours = false)
    {
        $dateTime = Carbon::parse($date)->addHours($hours);
        if($onlyHours){
            return $dateTime->toTimeString();
        }
        return $dateTime->toDateTimeString();
    }
}
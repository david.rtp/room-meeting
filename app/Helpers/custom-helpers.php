<?php


/**
 * @param $route
 * @return bool
 */
function route_is($route){
    return \Route::currentRouteName() == $route;
}
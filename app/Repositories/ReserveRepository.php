<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 03/06/18
 * Time: 17:34
 */

namespace App\Repositories;

use App\Models\MeetingRoom;
use App\Models\Reserve;
use App\Util\Dates;

class ReserveRepository
{
    use Dates;

    /**
     * @var Reserve
     */
    protected $model;
    protected $modelMeetingRoom;

    /**
     * ReserveRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Reserve();
        $this->modelMeetingRoom = new MeetingRoom();
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @return array
     */
    public function listHoursFreeAndBusy($dateStart, $dateEnd)
    {
        $dataReturn = [];
        $availableHours = [];
        $reserves = $this->getAllReserves($dateStart, $dateEnd);
        $reserves = $this->separateReservesByDate($reserves);
        $diffDays = $this->diffDays($dateStart, $dateEnd);
        for($day = 0; $day <= $diffDays; $day++){
            $date = $this->addDays($day, $dateStart);
            $theReserves = isset($reserves[$date]) ? $reserves[$date] : [];
            $availableHours[$date] = $this->_getAvailableReserves($theReserves);
            $this->_verifyHourIsAvailable($availableHours[$date]);
        }
        foreach ($availableHours as $date => $hours)
        {
            foreach ($hours as $minutes => $data){
                $startHour = $this->convertMinutesToHours($minutes);
                $endHour = $this->convertMinutesToHours($minutes + INTERVAL_RESERVES);
                $freeTime = true;
                if($data['status'] == RESERVE_STATUS_FREE){
                    if($this->isPast($date)) {
                        if($this->isToday($date)){
                            $_minutes = $this->convertHourToMinutes($this->now(true));
                            if($minutes < $_minutes){
                                $freeTime = false;
                            }
                        }else{
                            $freeTime = false;
                        }
                    }
                    if($freeTime){
                        array_push($dataReturn, [
                            'color' => COLOR_RESERVE_FREE,
                            'start' => "$date $startHour",
                            'end'   => "$date $endHour",
                            'title' => "Livre",
                            'type'  => RESERVE_STATUS_FREE
                        ]);
                    }
                }
                foreach ($data['reserves'] as $reserve){
                    array_push($dataReturn, [
                        'color'     => COLOR_RESERVE_BUSY,
                        'start'     => "$date $startHour",
                        'end'       => "$date $endHour",
                        'title'     => "{$reserve->user->name} ({$reserve->meetingRoom->name})",
                        'id'        => $reserve->id,
                        'type'      => RESERVE_STATUS_BUSY,
                        'reserve'   => $reserve,
                        'past'      => $this->isPast($reserve->date_time)
                    ]);
                }
            }
        }
        return $dataReturn;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRooms()
    {
        return $this->modelMeetingRoom->all();
    }

    /**
     * @param null $dateStart
     * @param null $dateEnd
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllReserves($dateStart = null, $dateEnd = null)
    {
        $reserves = $this->model->with(['meetingRoom', 'user']);
        if(!is_null($dateStart)){
            if(!is_null($dateEnd)){
                $reserves->whereBetween('date_time', [$dateStart, $dateEnd]);
            }else{
                $reserves->where('date_time', '>=', $dateStart);
            }
        }
        return $reserves->get();
    }

    /**
     * @param \App\Models\Reserve | \Illuminate\Database\Eloquent\Collection $reserves
     * @return array
     */
    public function separateReservesByDate($reserves)
    {
        $allReserves = [];
        foreach ($reserves as $reserve)
        {
            $allReserves[$this->convertDate($reserve->date_time, 'Y-m-d H:i:s', 'Y-m-d')][] = $reserve;
        }
        return $allReserves;
    }

    /**
     * @param \App\Models\Reserve | \Illuminate\Database\Eloquent\Collection $reserves
     * @return array
     */
    public function separateReservesByCurrentUser($reserves)
    {
        $allReserves = [];
        foreach ($reserves as $reserve){
            $allReserves[$reserve->user_id][] = $reserve;
        }
        return $allReserves;
    }

    /**
     * @description Este metodo é efetivo apenas se estivermos parseando por reservas da mesma data
     * Desta maneira é possível identificar se para aquela determinada data os horários que estão disponiveis
     * e indiponíveis
     * @param \App\Models\Reserve | \Illuminate\Database\Eloquent\Collection $reserves
     * @return array
     */
    private function _getAvailableReserves($reserves)
    {
        $hours = [];
        for($hour = HOUR_START; $hour <= HOUR_END; $hour += INTERVAL_RESERVES){
            $inHour = false;
            $currentReserve = null;
            foreach ($reserves as $reserve){
                // Convertendo a hora da reserva em minutos
                $hourReserve = $this->convertHourToMinutes($this->convertDate($reserve->date_time, 'Y-m-d H:i:s', 'H:i:s'));
                if($hour == $hourReserve)
                {
                    $inHour = true;
                    $currentReserve = $reserve;
                    break;
                }
            }
            if($inHour) {
                $hours[$hour][] = $currentReserve;
            }else {
                $hours[$hour] = [];
            }
        }
        return $hours;
    }

    /**
     * @param \App\Models\Reserve | \Illuminate\Database\Eloquent\Collection $reserves
     * @return array
     */
    public function separateReservesInMinutes($reserves)
    {
        $allReserves = [];
        foreach ($reserves as $reserve)
        {
            $hourReserve = $this->convertHourToMinutes($this->convertDate($reserve->date_time, 'Y-m-d H:i:s', 'H:i:s'));
            $allReserves[$hourReserve] = $reserve;
        }
        return $allReserves;
    }

    /**
     * Este metodo recebe todos os horários de um dia específico, todas as suas horas
     * com e sem agendamentos
     * @param $allHours
     */
    private function _verifyHourIsAvailable(&$allHours)
    {
        $totalRooms = $this->getRooms()->count();
        /**
         * Parseando pelos horários
         */
        foreach ($allHours as $key => $reserves)
        {
            $totalReserves = count($reserves);
            /**
             * Se o total de reservas for igual a quantidade de salas
             * quer dizer que não ha nenhum horário disponível
             */
            if($totalReserves == $totalRooms){
                $allHours[$key] = [
                    'status' => RESERVE_STATUS_BUSY,
                    'reserves' => $reserves
                ];
            }else {
                /**
                 * Caso contrário analisamos se há alguma reserva
                 * Não havendo, automaticamente determinamos este horário como livre
                 */
                if(empty($reserves)){
                    $allHours[$key] = [
                        'status' => RESERVE_STATUS_FREE,
                        'reserves' => []
                    ];
                }else {
                    /**
                     * Caso haja reservas, devemos verificar se o usuario atual ja reservou neste horário
                     */
                    $status = RESERVE_STATUS_FREE;
                    foreach ($reserves as $reserve){
                        // Se houver alguma reserva que pertence ao usuário definimos como horário ocupado
                        if($reserve->user_id == auth()->id()){
                            $status = RESERVE_STATUS_BUSY;
                        }
                    }
                    $allHours[$key] = [
                        'status' => $status,
                        'reserves' => $reserves
                    ];
                }
            }
        }
    }

    /**
     * @param $dateTime
     * @return \App\Models\Reserve
     */
    public function getRoomsAvailable($dateTime)
    {
        $rooms = $this->model->where('date_time', $dateTime)->pluck('meeting_room_id');
        $roomsAvailable = $this->modelMeetingRoom->whereNotIn('id', $rooms)->get();
        return $roomsAvailable;
    }

    /**
     * @param $dateTime
     * @param $roomId
     * @return mixed
     */
    public function reserveRoom($dateTime, $roomId)
    {
        return $this->model->create([
            'date_time'         => $dateTime,
            'meeting_room_id'   => $roomId,
            'user_id'           => auth()->user()->id
        ]);
    }

    /**
     * @param $roomId
     * @return mixed
     */
    public function reserveCancel($roomId)
    {
        return $this->model->where('id', $roomId)->where('user_id', auth()->id())->delete();
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingRoom extends Model
{
    use SoftDeletes;

    protected $table = 'meeting_rooms';
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reserves()
    {
        return $this->hasMany('App\Models\Reserve', 'meeting_room_id', 'id');
    }
}

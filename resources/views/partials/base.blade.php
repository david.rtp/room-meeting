<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
@include('partials.head')
<body>
<div id="app">
    @yield('content')
</div>
</body>
@include('partials.scripts')
</html>
@extends('partials.base')
@section('content')
<div class="container">
    <h2 class="text-center">Reserva de Salas de Reunião</h2>
    <div class="login-form">
        <form action="{{route('auth.login')}}" method="POST">
            <h4 class="text-center">Login</h4>
            @include('partials.messages')
            {{csrf_field()}}
            @include('auth.form')
            <button type="submit" class="btn btn-primary">Login</button>
        </form>

    </div>
</div>
@endsection
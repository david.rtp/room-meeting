@extends('restricted-area.partials.base')
@section('content')
    <div class="container" v-cloak>
        <h2>Reservas</h2>
        @include('partials.messages')

        <p>
            Para gerenciar suas reservas, basta clicar no item do calendário.
        </p>

        <the-reserve inline-template>
            <div>
                <full-calendar ref="calendar"
                               :event-sources="eventSources"
                               @event-created="eventCreated"
                               :config="config"
                               :default-view="defaultView">
                </full-calendar>
                @include('restricted-area.reserve.modal')
            </div>
        </the-reserve>
    </div>
@endsection
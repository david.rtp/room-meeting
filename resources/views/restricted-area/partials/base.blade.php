<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
@include('partials.head')
<body>
<div id="app">
    @include('restricted-area.partials.sidebar')
    @yield('content')
</div>
</body>
@include('partials.scripts')
</html>
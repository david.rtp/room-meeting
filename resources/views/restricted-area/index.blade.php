@extends('restricted-area.partials.base')
@section('content')
    <div class="container">
        <h2>Seja bem vindo {{auth()->user()->name}}!</h2>
        <p>Sistema de agendamento de salas de reunião.</p>
    </div>
@endsection
<table class="table user-list">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Email</th>
            <th scope="col">Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{route('restricted-area.user.edit', $user->id)}}" class="btn btn-primary">Editar</a>
                    {!! Form::open(['route' => ['restricted-area.user.destroy', $user->id], 'method' => 'DELETE']) !!}
                        <button class="btn btn-danger -btn-delete">Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center"> Nenhum usuário cadastrado </td>
            </tr>
        @endforelse
    </tbody>
</table>
{{$users->render()}}
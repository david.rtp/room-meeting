-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: reserves
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meeting_rooms`
--

DROP TABLE IF EXISTS `meeting_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meeting_rooms_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting_rooms`
--

LOCK TABLES `meeting_rooms` WRITE;
/*!40000 ALTER TABLE `meeting_rooms` DISABLE KEYS */;
INSERT INTO `meeting_rooms` VALUES (1,'Sala 1',NULL,'2018-06-02 23:19:31','2018-06-02 23:26:23'),(2,'Sala 2',NULL,'2018-06-02 23:28:27','2018-06-02 23:28:27'),(3,'Sala 3',NULL,'2018-06-02 23:28:39','2018-06-02 23:28:39');
/*!40000 ALTER TABLE `meeting_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_06_01_224053_create_meeting_rooms_table',1),(4,'2018_06_01_224155_create_reserves_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserves`
--

DROP TABLE IF EXISTS `reserves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned NOT NULL,
  `meeting_room_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reserves_user_id_foreign` (`user_id`),
  KEY `reserves_meeting_room_id_foreign` (`meeting_room_id`),
  CONSTRAINT `reserves_meeting_room_id_foreign` FOREIGN KEY (`meeting_room_id`) REFERENCES `meeting_rooms` (`id`),
  CONSTRAINT `reserves_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserves`
--

LOCK TABLES `reserves` WRITE;
/*!40000 ALTER TABLE `reserves` DISABLE KEYS */;
INSERT INTO `reserves` VALUES (1,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-03 22:08:17','2018-06-04 03:50:05'),(2,'2018-06-04 03:49:44',1,2,'2018-06-04 03:49:44','2018-06-03 22:08:33','2018-06-04 03:49:44'),(3,'2018-06-04 03:49:44',1,2,'2018-06-04 03:49:44','2018-06-03 22:08:37','2018-06-04 03:49:44'),(4,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-03 22:09:06','2018-06-04 03:50:05'),(5,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-03 22:09:12','2018-06-04 03:50:05'),(6,'2018-06-04 03:49:44',1,2,'2018-06-04 03:49:44','2018-06-04 01:46:07','2018-06-04 03:49:44'),(7,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 01:46:10','2018-06-04 03:50:05'),(8,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 01:46:13','2018-06-04 03:50:05'),(9,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 03:06:35','2018-06-04 03:50:05'),(10,'2018-06-04 03:49:44',1,2,'2018-06-04 03:49:44','2018-06-04 03:07:15','2018-06-04 03:49:44'),(11,'2018-06-04 03:49:44',1,2,'2018-06-04 03:49:44','2018-06-04 03:10:23','2018-06-04 03:49:44'),(12,'2018-06-08 03:00:00',1,3,NULL,'2018-06-04 03:10:48','2018-06-04 03:10:48'),(13,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 03:13:38','2018-06-04 03:50:05'),(14,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 03:17:14','2018-06-04 03:50:05'),(15,'2018-06-04 03:50:05',1,1,'2018-06-04 03:50:05','2018-06-04 03:18:27','2018-06-04 03:50:05'),(16,'2018-06-08 16:00:00',1,1,NULL,'2018-06-04 03:51:47','2018-06-04 03:51:47'),(17,'2018-06-08 17:00:00',1,3,NULL,'2018-06-04 03:52:00','2018-06-04 03:52:00'),(18,'2018-06-04 03:52:47',1,1,'2018-06-04 03:52:47','2018-06-04 03:52:20','2018-06-04 03:52:47'),(19,'2018-06-05 19:00:00',1,1,NULL,'2018-06-04 03:52:31','2018-06-04 03:52:31');
/*!40000 ALTER TABLE `reserves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrador','admin@teste.com','$2y$10$LaH9kVsLISmL8Nn9XaYTButMhAnfRAP/PrE4aNTa6J6YPFuBNLR7u','1','rQUljayYXc3WyP5KOVnou8Bk6lMMEaBgmvOO8Z3GekzUqPgq1QAzsz6VkLMl','2018-06-02 19:46:50','2018-06-02 19:46:50'),(2,'Nero Hermann','mozuguvy@mailinator.com.br','Pa$$w0rd!','2',NULL,'2018-06-02 22:03:24','2018-06-02 22:47:18'),(4,'Aretha Stewart','sawifypew@mailinator.net','Pa$$w0rd!','2',NULL,'2018-06-02 22:55:04','2018-06-02 22:55:04'),(5,'Yolanda Buckley','gohoqop@mailinator.com','Pa$$w0rd!','2',NULL,'2018-06-02 22:55:14','2018-06-02 22:55:14'),(6,'Odette Hendrix','nosybe@mailinator.com','554655','2',NULL,'2018-06-04 04:03:07','2018-06-04 04:03:07'),(7,'Isadora Cohen','xoter@mailinator.net','$2y$10$hiYGhaQ.Iywsr6COy0dYHevf6PqQe1DI3zOQaJx7zO6IxS0EtRO5K','2','xJpGWYQAC2jlYk5FV58pFUi1VGjCWTMI5XLNxpmLsN9G88feLButM6ONI0cW','2018-06-04 04:08:18','2018-06-04 04:08:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'reserves'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-04  1:24:16
